using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Digger
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // ��������
        private Rectangle view_port_rectangle;  // ������� �������� ����
        private Texture2D background;           // ��� �������� ����
        private Texture2D dig_right;
        private Texture2D dig_left;
        private Texture2D dig_down;
        private Texture2D dig_up;
        private Texture2D enemy_right;
        private Texture2D enemy_left;
        private Texture2D enemy_down;
        private Texture2D enemy_up;
        private Texture2D dirt;
        private Texture2D treasure;
        private Texture2D start_spr;
        private Texture2D about_spr;
        private Texture2D win_spr;
        private Texture2D loose_spr;

        // ������� �������
        private GameObject digger;
        private GameObject[] enemies;

        // ����������� ��������� ����
        private int block_size = 40;
        private int max_enemies = 3;
        private bool game_start = false;
        private bool game_win   = false;
        private bool game_loose = false;
        private bool game_about = false;

        // ������������ ��������� ����
        private float digger_speed = 5f;
        private float enemy_speed  = 2f;

        // ��������� ������
        private int[,] cur_level;
        private int treasure_count;
        private int enemies_num;
        private bool level_parsed = false;
        

        // �����-�����
        private int[,] test_level = new int [20, 20] {
        {0, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}};
        

        private int[,] level1 = new int [20, 20] {
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, //1
        {2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2}, //2
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, //3
        {4, 4, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3}, //4
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 3}, //5
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 3}, //6
        {2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 3}, //7
        {2, 2, 2, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 2, 2, 4, 3}, //8
        {2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 3}, //9
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 3}, //10
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 3}, //11
        {3, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 4, 3}, //12
        {2, 3, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 3, 4, 3}, //13
        {3, 2, 4, 2, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 4, 3, 4, 3}, //14
        {2, 3, 4, 2, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 4, 3, 4, 3}, //15
        {3, 2, 4, 2, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 4, 3, 4, 3}, //16
        {2, 3, 4, 2, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 4, 3, 4, 3}, //17
        {3, 2, 4, 2, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 4, 3, 4, 3}, //18
        {2, 3, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 4, 3}, //19
        {3, 2, 1, 2, 2, 2, 2, 2, 2, 0, 4, 2, 2, 2, 2, 2, 4, 1, 4, 3}};//20
        //  2     4     6     8    10    12    14    16    18    20

        private int[,] level2 = new int[20, 20] {
        //  2     4     6     8    10    12    14    16    18    20
        {2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2}, //1
        {2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4, 2, 2, 2, 2}, //2
        {2, 2, 2, 4, 4, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 4, 4, 2, 2, 2}, //3
        {2, 3, 2, 4, 2, 3, 3, 2, 2, 2, 2, 3, 3, 3, 2, 2, 4, 2, 3, 2}, //4
        {2, 3, 2, 4, 3, 2, 2, 3, 2, 2, 2, 3, 3, 3, 3, 2, 4, 2, 3, 2}, //5
        {2, 3, 2, 1, 2, 3, 3, 2, 2, 3, 3, 2, 3, 3, 2, 2, 1, 2, 3, 2}, //6
        {2, 3, 2, 4, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 3, 2, 4, 2, 3, 2}, //7
        {2, 2, 2, 4, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 2, 2, 2}, //8
        {2, 2, 2, 2, 4, 4, 2, 3, 3, 3, 3, 3, 3, 2, 4, 4, 2, 2, 2, 2}, //9
        {2, 2, 2, 2, 2, 4, 2, 3, 3, 3, 3, 3, 3, 2, 4, 2, 2, 2, 2, 2}, //10
        {2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2}, //11
        {2, 3, 3, 2, 2, 4, 4, 3, 3, 3, 4, 3, 3, 4, 4, 2, 2, 3, 3, 2}, //12
        {2, 3, 3, 2, 2, 2, 4, 3, 3, 3, 4, 3, 3, 4, 2, 2, 2, 3, 3, 2}, //13
        {2, 3, 3, 3, 2, 2, 4, 4, 4, 4, 0, 4, 4, 4, 2, 2, 3, 3, 3, 2}, //14
        {2, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 2}, //15
        {2, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 2}, //16
        {2, 3, 3, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 3, 3, 2}, //17
        {2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2}, //18
        {2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2}, //19
        {2, 2, 2, 2, 3, 3, 3, 3, 3, 2, 2, 3, 3, 3, 3, 3, 2, 2, 2, 2}};//20
        //  2     4     6     8    10    12    14    16    18    20

        // ����������� �������
        private int[,] ArrayCopy(int[,] from)
        {
            int[,] to = new int[20, 20];
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    to[i, j] = from[i, j];
                }
            }
            return to;
        }

        // ��������� ����� ������
        private void PrepareLevel(int[,] l)
        {
            // ������������� ������
            digger = new GameObject(dig_up);

            // ������������� ������
            enemies = new GameObject[max_enemies];
            for (int i = 0; i < max_enemies; i++)
            {
                enemies[i] = new GameObject(enemy_up);
            }
            level_parsed = true;
            treasure_count = 0;
            enemies_num = 0;
            for (int i = 0; i < max_enemies; i++)
            {
                enemies[i].IsAlive = false;
            }
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    // ����� ��������� ������� ������
                    if (l[j, i] == 0)
                    {
                        digger.SetParam(i, j, 0);
                    }
                    if (l[j, i] == 3)
                    {
                        treasure_count++;
                    }
                    if (l[j, i] == 1)
                    {
                        if (enemies_num <= max_enemies)
                        {
                            enemies[enemies_num].SetParam(i, j, 1);
                            enemies_num++;
                        }
                    }
                }
            }
        }

        private Vector3 TargetEnemyMax(GameObject e)
        {
            int i, j, d;
            int dx = (int)(digger.id.X - e.id.X);
            int dy = (int)(digger.id.Y - e.id.Y);
            int adx = Math.Abs((int)(dx));
            int ady = Math.Abs((int)(dy));
            if (ady > adx)
            {
                i = 0;
                if (dy >= 0)
                {
                    // ����
                    j = 1;
                    d = 0;
                }
                else
                {
                    // �����
                    j = -1;
                    d = 1;
                }
            }
            else
            {
                j = 0;
                if (dx >= 0)
                {
                    // ������
                    i = 1;
                    d = 2;
                }
                else
                {
                    // �����
                    i = -1;
                    d = 3;
                }
            }
            return new Vector3(i, j, d);
        }

        private Vector3 TargetEnemyMin(GameObject e)
        {
            int i, j, d;
            int dx = (int)(digger.id.X - e.id.X);
            int dy = (int)(digger.id.Y - e.id.Y);
            int adx = Math.Abs((int)(dx));
            int ady = Math.Abs((int)(dy));
            if (ady < adx)
            {
                i = 0;
                if (dy >= 0)
                {
                    // ����
                    j = 1;
                    d = 0;
                }
                else
                {
                    // �����
                    j = -1;
                    d = 1;
                }
            }
            else
            {
                j = 0;
                if (dx >= 0)
                {
                    // ������
                    i = 1;
                    d = 2;
                }
                else
                {
                    // �����
                    i = -1;
                    d = 3;
                }
            }
            return new Vector3(i, j, d);
        }

        private bool CheckDirection(GameObject e, Vector3 v)
        {
            int i = (int)v.X;
            int j = (int)v.Y;
            // ���������, ����� �� �� ��������� � �������� �����������
            if (!((e.id.X + i == -1) || (e.id.X + i == 20) || (e.id.Y + j == -1) || (e.id.Y + j == 20)))
            {
                int type = cur_level[(int)(e.id.Y + j), (int)(e.id.X + i)];
                if ((type == 0) || (type == 4))
                {
                    return true;
                }
            }
            return false;
        }

        private int ReflectDir(int t)
        {
            switch (t)
            {
                case (0):
                    return 1;
                case (1):
                    return 0;
                case (2):
                    return 3;
                case (3):
                    return 2;
                default:
                    return 4;
            }
        }

        private Vector3 EnemyBehaviorA(GameObject e)
        {
            Vector3 pos;
            pos = TargetEnemyMax(e);
            if (CheckDirection(e, pos) )
            {
                return pos;
            }
            pos = TargetEnemyMin(e);
            if (CheckDirection(e, pos))
            {
                return pos;
            }
            pos = TargetEnemyMax(e);
            pos.X = -pos.X;
            pos.Y = -pos.Y;
            pos.Z = ReflectDir((int)pos.Z);
            if (CheckDirection(e, pos))
            {
                return pos;
            }
            pos = TargetEnemyMin(e);
            pos.X = -pos.X;
            pos.Y = -pos.Y;
            pos.Z = ReflectDir((int)pos.Z);
            if (CheckDirection(e, pos))
            {
                return pos;
            }
            return Vector3.Zero;
        }

        private Vector3 EnemyBehaviorB(GameObject e)
        {
            Vector3 pos;
            pos = TargetEnemyMin(e);
            if (CheckDirection(e, pos))
            {
                return pos;
            }
            pos = TargetEnemyMax(e);
            if (CheckDirection(e, pos))
            {
                return pos;
            }
            pos = TargetEnemyMin(e);
            pos.X = -pos.X;
            pos.Y = -pos.Y;
            pos.Z = ReflectDir((int)pos.Z);
            if (CheckDirection(e, pos))
            {
                return pos;
            }
            pos = TargetEnemyMax(e);
            pos.X = -pos.X;
            pos.Y = -pos.Y;
            pos.Z = ReflectDir((int)pos.Z);
            if (CheckDirection(e, pos))
            {
                return pos;
            }
            return Vector3.Zero;
        }

        private void UpdateDigger()
        {
            int i = 0, j = 0;
            if (digger.InMove)
            {
                if (digger.Position == digger.Target)
                {
                    digger.InMove = false;
                    digger.Target = new Vector2(-100, -100);
                    digger.Velocity = new Vector2(0, 0);
                    digger.direction = 4;
                }
                else
                {
                    digger.Position.X += digger.Velocity.X;
                    digger.Position.Y += digger.Velocity.Y;
                }
            }
            else
            {
                switch (digger.direction)
                {
                    // ����
                    case (0):
                        i = 0;
                        j = 1;
                        break;
                    // �����
                    case (1):
                        i = 0;
                        j = -1;
                        break;
                    // ������
                    case (2):
                        i = 1;
                        j = 0;
                        break;
                    // �����
                    case (3):
                        i = -1;
                        j = 0;
                        break;
                    default:
                        i = 0;
                        j = 0;
                        break;
                }
                // ���������, ����� �� �� ��������� � �������� �����������
                if (!((digger.id.X + i == -1) || (digger.id.X + i == 20) || (digger.id.Y + j == -1) || (digger.id.Y + j == 20)))
                {
                    if (cur_level[(int)(digger.id.Y + j), (int)(digger.id.X + i)] == 2)
                    {
                        cur_level[(int)(digger.id.Y + j), (int)(digger.id.X + i)] = 0;
                    }
                    else if (cur_level[(int)(digger.id.Y + j), (int)(digger.id.X + i)] == 3)
                    {
                        cur_level[(int)(digger.id.Y + j), (int)(digger.id.X + i)] = 0;
                        treasure_count--;
                    }
                    digger.Target = new Vector2((digger.id.X + i) * block_size, (digger.id.Y + j) * block_size);
                    digger.Velocity = new Vector2(i * digger_speed, j * digger_speed);
                    cur_level[(int)(digger.id.Y), (int)(digger.id.X)] = 4;
                    digger.id = new Vector2(digger.id.X + i, digger.id.Y + j);
                    digger.InMove = true;
                }
            }
        }

        private void UpdateEnemySpr(GameObject e)
        {
            switch (e.direction)
            {
                // ����
                case (0):
                    e.Sprite = enemy_down;
                    break;
                // �����
                case (1):
                    e.Sprite = enemy_up;
                    break;
                // ������
                case (2):
                    e.Sprite = enemy_right;
                    break;
                // �����
                case (3):
                    e.Sprite = enemy_left;
                    break;
            }
        }

        private void UpdateEnemy( int pos)
        {
            int i = 0, j = 0;
            Vector3 v;
            GameObject e = enemies[pos];
            if (e.InMove)
            {
                if (e.Bounds.Intersects(digger.Bounds))
                {
                    game_loose = true;
                    game_start = false;
                    level_parsed = false;
                }
                else if (e.Position == e.Target)
                {
                    e.InMove = false;
                    e.Target = new Vector2(-100, -100);
                    e.Velocity = new Vector2(0, 0);
                    e.direction = 4;
                }
                else
                {
                    e.Position.X += e.Velocity.X;
                    e.Position.Y += e.Velocity.Y;
                }
            }
            else
            {
                if ((pos == 0) || (pos == 2))
                {
                    v = EnemyBehaviorA(e);
                }
                else
                {
                    v = EnemyBehaviorB(e);
                }
                i = (int)v.X;
                j = (int)v.Y;
                // ���� �� ����� �� �����
                if (v.Z != 4)
                {
                    e.Target = new Vector2((e.id.X + i) * block_size, (e.id.Y + j) * block_size);
                    e.Velocity = new Vector2(i * enemy_speed, j * enemy_speed);
                    cur_level[(int)(e.id.Y), (int)(e.id.X)] = 4;
                    e.id = new Vector2(e.id.X + i, e.id.Y + j);
                    cur_level[(int)(e.id.Y), (int)(e.id.X)] = 1;
                    e.direction = (int)v.Z;
                    e.InMove = true;
                    UpdateEnemySpr(e);
                }
            }
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // ������ ������ ����
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 800;
            graphics.ApplyChanges();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            spriteBatch = new SpriteBatch(GraphicsDevice);
            view_port_rectangle = new Rectangle(0, 0,
            graphics.GraphicsDevice.Viewport.Width,
            graphics.GraphicsDevice.Viewport.Height);

            background = Content.Load<Texture2D>(@"background");
            dig_left  = Content.Load<Texture2D>(@"dig_left");
            dig_right = Content.Load<Texture2D>(@"dig_right");
            dig_down  = Content.Load<Texture2D>(@"dig_down");
            dig_up    = Content.Load<Texture2D>(@"dig_up");
            enemy_left  = Content.Load<Texture2D>(@"enemy_left");
            enemy_right = Content.Load<Texture2D>(@"enemy_right");
            enemy_down  = Content.Load<Texture2D>(@"enemy_down");
            enemy_up    = Content.Load<Texture2D>(@"enemy_up");
            dirt = Content.Load<Texture2D>(@"dirt");
            treasure = Content.Load<Texture2D>(@"treasure");
            start_spr = Content.Load<Texture2D>(@"start");
            about_spr = Content.Load<Texture2D>(@"about");
            win_spr = Content.Load<Texture2D>(@"win");
            loose_spr = Content.Load<Texture2D>(@"loose");

            //// ������������� ������
            //digger = new GameObject(dig_up);

            //// ������������� ������
            //enemies = new GameObject[max_enemies];
            //for (int i = 0; i < max_enemies; i++)
            //{
            //    enemies[i] = new GameObject(enemy_up);
            //}

        }

        protected override void UnloadContent()
        {
            
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            KeyboardState keyboardState = Keyboard.GetState();

            if (!game_start)
            {
                if (keyboardState.IsKeyDown(Keys.D1))
                {
                    cur_level = ArrayCopy(level1);
                    //cur_level = level1;
                    game_start = true;
                }
                if (keyboardState.IsKeyDown(Keys.D2))
                {
                    cur_level = ArrayCopy(level2);
                    //cur_level = level2;
                    game_start = true;
                }
                if (keyboardState.IsKeyDown(Keys.T))
                {
                    cur_level = ArrayCopy(test_level);
                    //cur_level = test_level;
                    game_start = true;
                }
                if (keyboardState.IsKeyDown(Keys.E))
                {
                    this.Exit();
                }
                if (keyboardState.IsKeyDown(Keys.Space))
                {
                    game_about = true;
                }
            }

            if (game_about)
            {
                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    game_about = false;
                }
            }

            if (game_win)
            {
                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    game_win = false;
                }
            }

            if (game_loose)
            {
                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    game_loose = false;
                }
            }

            if (game_start)
            {
                if (!level_parsed)
                {
                    PrepareLevel(cur_level);
                }

                if (!digger.InMove)
                {
                    // ������� ������� ������
                    if (keyboardState.IsKeyDown(Keys.Right) || keyboardState.IsKeyDown(Keys.D))
                    {
                        digger.direction = 2;
                        digger.Sprite = dig_right;
                    }

                    // ������� ������� �����
                    if (keyboardState.IsKeyDown(Keys.Left) || keyboardState.IsKeyDown(Keys.A))
                    {
                        digger.direction = 3;
                        digger.Sprite = dig_left;
                    }

                    // ������� ������� �����
                    if (keyboardState.IsKeyDown(Keys.Up) || keyboardState.IsKeyDown(Keys.W))
                    {
                        digger.direction = 1;
                        digger.Sprite = dig_up;
                    }

                    // ������� ������� ����
                    if (keyboardState.IsKeyDown(Keys.Down) || keyboardState.IsKeyDown(Keys.S))
                    {
                        digger.direction = 0;
                        digger.Sprite = dig_down;
                    }
                }
                if (digger.direction != 4)
                {
                    UpdateDigger();
                }

                for (int i = 0; i < enemies_num; i++)
                {
                    UpdateEnemy(i);
                }

                if (treasure_count == 0)
                {
                    game_start = false;
                    game_win = true;
                    level_parsed = false;
                }

                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    game_start = false;
                    level_parsed = false;
                }
            }      

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            if (!game_start)
            {
                spriteBatch.Draw(start_spr, view_port_rectangle, Color.White);
            }

            if (game_about)
            {
                spriteBatch.Draw(about_spr, view_port_rectangle, Color.White);
            }

            if (game_win)
            {
                spriteBatch.Draw(win_spr, view_port_rectangle, Color.White);
            }

            if (game_loose)
            {
                spriteBatch.Draw(loose_spr, view_port_rectangle, Color.White);
            }

            if (game_start)
            {
                // ������ ���
                spriteBatch.Draw(background, view_port_rectangle, Color.White);

                // ������������ �����
                for (int i = 0; i < 20; i++)
                {
                    for (int j = 0; j < 20; j++)
                    {
                        Rectangle r = new Rectangle((i * block_size), (j * block_size), block_size, block_size);
                        if (cur_level[j, i] == 2)
                        {
                            spriteBatch.Draw(dirt, r, Color.White);
                        }
                        if (cur_level[j, i] == 3)
                        {
                            spriteBatch.Draw(treasure, r, Color.White);
                        }
                    }
                }

                // ������ �������
                spriteBatch.Draw(digger.Sprite, digger.Position, Color.White);

                // ������ ������
                for (int i = 0; i < max_enemies; i++)
                {
                    if (enemies[i].IsAlive)
                    {
                        spriteBatch.Draw(enemies[i].Sprite, enemies[i].Position, Color.White);
                    }
                }
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
