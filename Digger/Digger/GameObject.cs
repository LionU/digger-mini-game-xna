﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Digger
{
    public class GameObject
    {
        public Texture2D Sprite { get; set; }               // Спрайт
        /** Направление движения
         * 0 - Вниз
         * 1 - Ввпех
         * 2 - Вправо
         * 3 - Влево
         * 4 - Нет направления
         */
        public int direction;

        /** Тип объекта
         * 0 - Диггер
         * 1 - Противник
         * 2 - Земля
         * 3 - Сокровище
         * 4 - Пустота
         */
        public int type; 

        public Vector2 id;
        public Vector2 Position;                            // Положение
        public Vector2 Velocity;                            // Скорость
        public Vector2 Target;                              // Точка назначения
        public int Width { get { return Sprite.Width; } }   // Ширина
        public int Height { get { return Sprite.Height; } } // Высота
        public bool IsAlive { get; set; }                   // Жив ли обьект 
        public bool InMove  { get; set; }                   // Юнит в движении
        public Rectangle Bounds                             // Границы обьекта
        {
            get
            {
                return new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
            }
        }

        public GameObject(Texture2D sprite)
        {
            
            direction = 4;
            Sprite = sprite;
            IsAlive = false;
            InMove  = false;
            id = Vector2.Zero;
            Velocity = Vector2.Zero;
            Target = new Vector2(-100, -100);
            Position = Vector2.Zero;
        }

        public void SetParam(int i, int j, int Type)
        {
            IsAlive = true;
            id = new Vector2(i, j);
            type = Type;
            Position = new Vector2(Sprite.Width * i, Sprite.Height * j);
        }
    }
}
